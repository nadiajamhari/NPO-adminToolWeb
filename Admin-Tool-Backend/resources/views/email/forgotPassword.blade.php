<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Reset Password Email</title>
</head>
<body>
<div>
    <p>Hi, {{$name}}</p>
    <p>Resetting your password is easy. Just press the link below and follow the instructions. We'll have you up and running in no time.</p>
    <a href="http://localhost:3000/reset-password/{{$token}}">Reset Password</a>
    <br>
    <p>Regards</p>
    <p>Cinta Syria Malaysia</p>
</div>
</body>
</html>