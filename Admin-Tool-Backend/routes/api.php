<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\DownloadController;

#region Auth
use App\Http\Controllers\AuthController;
#endregion

#region User
use App\Http\Controllers\UserController;
use App\Http\Controllers\UsersRoleController;
#endregion

#region Address
use App\Http\Controllers\AddressesStateController;
#endregion

#region tax exemption
use App\Http\Controllers\TaxExemptionController;
use App\Http\Controllers\TaxExemptionsTypeController;
use App\Http\Controllers\TaxExemptionsPostageController;
use App\Http\Controllers\TaxExemptionsStatusController;
#endregion

#region asset
use App\Http\Controllers\AssetController;
use App\Http\Controllers\AssetQrcodeController;
use App\Http\Controllers\AssetStatusController;
use App\Http\Controllers\AssetCategoryController;
use App\Http\Controllers\AssetLogController;
use App\Http\Controllers\AssetItemController;
#endregion

#region statusColor
use App\Http\Controllers\StatusColorController;
#endregion

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Public route
//Auth
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::post('/forgotPassword', [AuthController::class,'forgotPasswordEmail'])->name('email');
Route::post('/resetPassword', [AuthController::class, 'updatePasswordByToken']);

//TaxExemption
Route::post('/storeTax', [TaxExemptionController::class, 'store']);
Route::get('/getTypeTax', [TaxExemptionsTypeController::class, 'index']);
Route::get('/getPostage', [TaxExemptionsPostageController::class, 'index']);

//User Role
Route::post('/storeRole', [UsersRoleController::class, 'store']);

//State
Route::get('/allState', [AddressesStateController::class,'index']);
Route::get('/exportExcelTax', [TaxExemptionController::class, 'export']);

// Protected routes - need to be authenticated
Route::group(['middleware'=>['auth:sanctum']], function () {
    
    Route::post('/download/{url}',[DownloadController::class,'download']);

    #region Auth
    Route::post('/logout', [AuthController::class, 'logout']);
    #endregion

    #region TaxExemption
    Route::get('/taxExemptions', [TaxExemptionController::class, 'index']);
    Route::get('/statusTaxExemptions', [TaxExemptionsStatusController::class, 'index']);
    Route::delete('/deleteTax/{id}', [TaxExemptionController::class, 'delete']);
    Route::delete('/deleteTaxs/{ids}', [TaxExemptionController::class, 'deleteAll']);
    Route::get('/taxDetails/{id}' ,[TaxExemptionController::class, 'details']);
    Route::put('/updateStatusTax/{id}', [TaxExemptionController::class, 'updateStatus']);
    Route::put('/updateStatusTaxs/{ids}', [TaxExemptionController::class, 'updateStatusAll']);
    #endregion   

    #region User
    Route::get('/allUsers', [UserController::class, 'index']);
    Route::delete('/deleteUser/{id}', [UserController::class, 'delete']);
    Route::post('/addNewUser', [UserController::class, 'registerUser']);
    Route::put('/updateUser/{id}', [UserController::class, 'update']);

    //User Role
    Route::get('/getRole', [UsersRoleController::class, 'getRole']);
    #endregion

    #region Address
    Route::post('/storeState', [AddressesStateController::class,'store']);
    #endregion
 
    #region Assets
    // Assets
    // Route::resource('assets', AssetController::class);
    Route::get('/getAsset', [AssetController::class, 'index']);
    Route::post('/addAsset', [AssetController::class, 'store']);
    Route::get('/getAsset/{id}', [AssetController::class, 'show']);
    Route::put('/updateAsset/{id}', [AssetController::class, 'update']);
    Route::delete('/deleteAsset/{id}', [AssetController::class, 'destroy']);
    Route::get('/searchAsset/{name}', [AssetController::class, 'search']);
    Route::get('/assetDetails', [AssetController::class, 'getDetails']);

    Route::get('/getCategory', [AssetCategoryController::class, 'index']);
    Route::get('/getCategory/{id}', [AssetCategoryController::class, 'show']);
    Route::post('/addCategory', [AssetCategoryController::class, 'store']);
    Route::delete('/deleteCategory/{id}', [AssetCategoryController::class, 'destroy']);
    Route::put('/updateCategory/{id}', [AssetCategoryController::class, 'update']);
    Route::get('/getCategoryAsset', [AssetCategoryController::class, 'showAsset']);

    Route::get('/getStatus', [AssetStatusController::class, 'index']);

    Route::post('/generateQrcode', [AssetQrcodeController::class, 'generate']);
    Route::get('/getQrcode', [AssetQrcodeController::class, 'index']);
    Route::get('/getQrcode/{id}', [AssetQrcodeController::class, 'show']);
    Route::delete('/deleteQrcode/{id}', [AssetQrcodeController::class, 'destroy']);
    
    Route::resource('assetItem', AssetItemController::class);

    Route::post('/logRequest', [AssetLogController::class, 'request']);
    Route::put('/logReturn/{id}', [AssetLogController::class, 'return']);
    Route::get('/getLog/{id}', [AssetLogController::class, 'show']);
    Route::get('/getLog', [AssetLogController::class, 'index']);
    #endregion

     #regionStatus
     Route::get('/statusColor',[StatusColorController::class,'index']);
     Route::put('/updateStatusTaxRequest/{id}', [TaxExemptionsStatusController::class,'update']);
     Route::post('/addStatusTaxRequest',[TaxExemptionsStatusController::class,'add']);
     #endregion
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
