<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxExemptionsDonor extends Model
{   
    protected $table = 'tax_exemptions_donors';
    protected $primaryKey ='id';
    protected $fillable = [
        'donorId',
      ];
}
