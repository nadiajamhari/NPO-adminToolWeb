<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssetItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'item_number',
        'item_price',
        'asset_id',
        'status_id',
        'qrcode_id',
    ];
}
