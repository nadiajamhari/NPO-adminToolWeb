<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxExemptionsStatus extends Model
{
    protected $table = 'tax_exemptions_statuses';
    protected $primaryKey ='id';
    protected $fillable = [
        'status',
        'colorId',
      ];
      public function StatusColor()
      {
          return $this->belongsTo(StatusColor::class , 'colorId');
      }
}