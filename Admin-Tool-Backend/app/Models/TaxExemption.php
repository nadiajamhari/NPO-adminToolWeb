<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxExemption extends Model
{
    protected $table = 'tax_exemptions';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'phoneNumber',
        'email',
        'amount',
        'fileId',
        'typeId',
        'statusId',
        'postId',
        'donorId',
        'addressId',
        'postageAddressId',
    ];
    
    //one tax has 1 file
    public function File()
    {
        return $this->belongsTo(File::class , 'fileId');
    }

    //one tax has 1 type of tax
    public function TaxExemptionsType()
    {
        return $this->belongsTo(TaxExemptionsType::class , 'typeId');
    }

    //one tax has 1 type of current status
    public function TaxExemptionsStatus()
    {
        return $this->belongsTo(TaxExemptionsStatus::class , 'statusId');
    }

    //one tax has 1 current of postage status
    public function TaxExemptionsPostage()
    {
        return $this->belongsTo(TaxExemptionsPostage::class , 'postId');
    } 

    //one tax has one details of donor (personal or company details)
    public function TaxExemptionsDonor()
    {
        return $this->belongsTo(TaxExemptionsDonor::class , 'donorId');
    }
     
    //one tax has one address
    public function Addresses()
    {
        return $this->belongsTo(Addresses::class , 'addressId');
    }

    //one tax has one postage address if applicable
    public function PostageAddresses()
    {
        return $this->belongsTo(Addresses::class , 'postageAddressId');
    }
    
}
