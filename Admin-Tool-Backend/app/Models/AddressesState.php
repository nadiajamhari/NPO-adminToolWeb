<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddressesState extends Model
{   
    protected $table = 'addresses_states';
    protected $primaryKey ='id';
    protected $fillable = [
      'state'
    ];
}
