<?php

namespace App\Exports;

use App\Models\TaxExemption;
use Maatwebsite\Excel\Concerns\FromCollection;

class TaxExemptionsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return TaxExemption::all();
    }
}
