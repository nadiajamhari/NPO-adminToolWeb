<?php

namespace App\Http\Controllers;
use App\Models\TaxExemptionsStatus;

use Illuminate\Http\Request;

class TaxExemptionsStatusController extends Controller
{
    public function index(){

        $data = TaxExemptionsStatus::with('StatusColor')->get();;
        return $data;
        
    }

    public function update(Request $request, $id){
        $fields = $request->validate([
            'status'  =>'required',
            'colorId' => 'required',
        ]);       
        $color = TaxExemptionsStatus::find($id);
        $color->status = $fields['status'];
        $color->save();
        $color->colorId = $fields['colorId'];
        $color->save();

        return $color;

    }

    public function add(Request $request){
        $fields = $request->validate([
            'status'  =>'required',
            'colorId' => 'required',
        ]);        
        
        $data = TaxExemptionsStatus::create($request->all());
        return $data;
    }
}
