<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mails;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Mail;

class AuthController extends Controller
{
    public function register(Request $request) {

        //assuming that this is registration for first Admin CSM (because using API)
        $fields = $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|unique:users,email',
            'password' => 'required|string|confirmed'
        ]);

        $user = User::create([
            'name' => $fields['name'],
            'email' => $fields['email'],
            'password' => bcrypt($fields['password']),
            'roleId' => 1, //Role id always be one for the first time register
        ]); 

        $token = $user->createToken('myapptoken')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response, 201);
    }

    public function logout(Request $request) {
        auth()->user()->tokens()->delete();
        return [
            'message' => 'Logged out'
        ];
    }

    public function login(Request $request) {
        $fields = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

        // Check email
        $user = User::where('email', $fields['email'])->first();

        // Check password
        if(!$user || !Hash::check($fields['password'], $user->password)) {
            return response([
                'message' => 'Bad creds'
            ], 401);
        }

        $token = $user->createToken('myapptoken')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response, 201);
    }

    public function forgotPasswordEmail (Request $request){

        $fields = $request->validate([
            'email' => 'required|email',
        ]);
        
        if(!$user = User::where('email', $fields['email'])->first()){
            return response()->json(['error' => 'User doesn\'t exists!'], 401);
        }

        $token = Str::random(60);

        DB::table('password_resets')->insert(
            ['email' => $fields['email'], 'token' => $token, 'created_at' => Carbon::now()]
        );



        Mail::send('email.forgotPassword', ['token' => $token , 'name' => $user->name], function($message) use ($request , $fields) {
                  $message->from($request->email);
                  $message->to($fields['email']);
                  $message->subject('Reset Password Notification1');
               });
        $response = [
            'token' => $token,
            'message' => 'We have e-mailed your password reset link!',
        ];

        return response($response, 201);
    }

    public function updatePasswordByToken (Request $request) {

        $fields = $request->validate([
            'token' => 'required',
            'password' => 'required|string'
        ]);

        $query = DB::table('password_resets')->where('token', $fields['token']);
        $passwordResets= $query->first();

        if(!$passwordResets){

            return response()->json(['error' => 'Invalid Token'], 401);
        }

        /** @var User $user */

        if(!$user = User::where('email', $passwordResets->email)->first()){
            return response()->json(['error' => 'User doesn\'t exists!'], 401);
        }

        $user->password = bcrypt($fields['password']);
        $user->save();

        $query ->delete();
        $response = [
            'message' => 'Successfuly change your password. You will redirect to login page in few seconds',
        ];

        return response($response, 201);
    }

    

}
