<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UsersRole;
use Illuminate\Support\Facades\DB;

class UsersRoleController extends Controller
{
    public function store(Request $request){

        $fields = $request->validate([
            'role' => 'string|required',
        ]);
        
        $data = UsersRole::create([
            'role' => $fields['role'],
        ]);
    }

    public function getRole() {

        $data = UsersRole::all();
        return response($data, 201);
    }
}
