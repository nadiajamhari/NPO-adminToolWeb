<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UsersRole;

class UserController extends Controller
{
    //get all list of user for user.js
    public function index(){

        $data = User::all();
        return response($data, 201);
    }

    public function delete($id){
        $data = User::findOrFail($id);
        $data->delete();
        return 204;
    }

    public function registerUser(Request $request){

        $fields = $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|unique:users,email',
            'roleId' => 'integer|required',
            'staffId'=> 'string|nullable',
        ]);
        
        $user = User::create([
            'name' => $fields['name'],
            'email' => $fields['email'],
            'password' => bcrypt("!Abc123"),
            'roleId' => $fields['roleId'],
            'staffId'=>$fields['staffId'],
        ]); 

        $token = $user->createToken('myapptoken')->plainTextToken;

        $response = "Succesfully Add";

        return response($response);
    }

    public function update (Request $request, $id) {
        $data = User::findOrFail($id);
        $data->update($request->all());
        $message = "Succesfully Save";
        return response($message);
    }
}
