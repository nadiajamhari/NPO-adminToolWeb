<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TaxExemptionsPostage;

class TaxExemptionsPostageController extends Controller
{
    public function index(){
        
        $data = TaxExemptionsPostage::all();
        return $data;
    }
}
