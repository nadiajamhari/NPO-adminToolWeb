<?php

namespace App\Http\Controllers;

use App\Models\AssetQrcode;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class AssetQrcodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return AssetQrcode::all();
    }

    /**
     * Generate QR code for the specified resource.
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function generate(Request $request)
    {   
        $request -> validate([
            'code'=> 'required',
        ]);
        $imageName = $request->code.'.svg'; 
        $destinationPath = storage_path('app\public\images\\'.$imageName);
        QrCode::format('svg')
                 ->size(400)
                 ->generate($request->code, $destinationPath);
        $url= "http://127.0.0.1:8000/storage/images/".$imageName;
        $data = ['src' => $url];  

        /* Store $imageName name in DATABASE from HERE*/ 
        $new = AssetQrcode::create($data);
        
        return $new->id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return AssetQrcode::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $qrcode = AssetQrcode::find($id);
        $qrcode->update($request->all());
        return $qrcode;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return AssetQrcode::destroy($id);
    }
}
