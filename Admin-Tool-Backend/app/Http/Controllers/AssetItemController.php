<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Models\AssetItem;
use App\Models\AssetQrcode;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class AssetItemController extends Controller
{
    public function index()
    {
        return AssetItem::all();
    }

    public function store(Request $request)
    {   
        $request -> validate([
            'asset_id' => 'required',
            'item_price' => 'required',
            'status_id' => 'required',
        ]);

        $asset = Asset::where('id', $request->asset_id)->get();
        $prefix = $asset[0]->asset_prefix;
        $number = AssetItem::where('asset_id', $request->asset_id)->max('item_number')+1;
        $code = $prefix.str_pad($number, 3, '0', STR_PAD_LEFT);
        
        // generate QRCode
        $imageName = $code.'.svg'; 
        $destinationPath = storage_path('app\public\images\\'.$imageName);
        QrCode::format('svg')
                 ->size(400)
                 ->generate($code, $destinationPath);
        $url= "http://127.0.0.1:8000/storage/images/".$imageName;
        $data = ['src' => $url];  
        $newQrcode = AssetQrcode::create($data);
        $qrcode_id = $newQrcode->id;
        
        return AssetItem::create($request->all() + ['item_number'=>$number,'qrcode_id' => $qrcode_id]);
    }

    public function show($id)
    {
        return AssetItem::find($id);
    }

    public function update(Request $request, $id)
    {
        $asset = AssetItem::find($id);
        $asset->update($request->all());
        return $asset;
    }

    public function destroy($id)
    {   
        $qr_id = AssetItem::find($id)->qrcode_id;
        AssetItem::destroy($id);
        AssetQrcode::destroy($qr_id);
        return response('deleted', 200);
    }
}
