<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TaxExemption;
use App\Models\Addresses;
use App\Models\File;
use App\Models\TaxExemptionsStatus;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Exports\TaxExemptionsExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class TaxExemptionController extends Controller
{
    public function index() {
        $data = TaxExemption::with('TaxExemptionsStatus', 'TaxExemptionsType')->get();
        return $data;
    }

    public function details($id){

        $data = TaxExemption::with('TaxExemptionsType','File','TaxExemptionsPostage','TaxExemptionsDonor')->get()->find($id);
        $status=TaxExemptionsStatus::with('StatusColor')->get()->find($data->statusId);
        $address = Addresses::with('AddressesState')->get()->find($data->addressId);
        $addressShipping=Addresses::with('AddressesState')->get()->find($data->postageAddressId);
        $file=File::find($data->fileId);
        $url = asset($file->file_path);
        $response = [
            'tax' => $data,
            'address' =>$address,
            'addressShipping'=> $addressShipping,
            'fileUrl' =>$url,
            'status'=>$status,
        ];
        return $response;
    }

    public function store(Request $request) {

        // return "hello";
        $fields = $request->validate([
            'name' => 'string|required',
            'phoneNumber' => 'string|required',
            'email' => 'string|required',
            'address'=>'string|required',
            'city'=>'string|required',
            'stateId'=>'string|required',       
            'postalCode' => 'string|required',           
            'shippingId' => 'string|required',
            'shippingAddress' => 'nullable|string',
            'shippingCity' => 'nullable|string',
            'shippingStateId' => 'nullable|integer',
            'shippingPostalCode'=>'nullable|string',
            'donorId' =>'required|string',
            'amount'=>'string|required',
            'filePath'=>'mimes:csv,txt,xlx,xls,pdf,png,jpeg|required',
            'typeId' => 'string|required',
            'postId' => 'string|required',
        ]);
        
        
        // $file      = $request->filePath;
        // $filename  = $file->getClientOriginalName();
        // $extension = $file->getClientOriginalExtension();
        // $receipt = time().'_'.$filename;  
        // $request->filePath->store('public');
        // $filePath = '/storage/' . $file;
        //store file
        $file      = $request->filePath;
        $filename  = $file->getClientOriginalName();
        // $extension = $file->getClientOriginalExtension();
        $receipt = time().'_'.$filename;  
        $file->storeAs('public/donor-receipt', $receipt);
        $filePath = '/storage/donor-receipt/' . $receipt;
        $fileId = DB::table('files')->insertGetId(
            ['file_path' =>  $filePath, 'created_at' => Carbon::now()]
        );
  
        //store address
        $addressId = DB::table('addresses')->insertGetId(
            [
                'address' =>  $fields['address'],
                'postalCode' => $fields['postalCode'],
                'city' => $fields['city'],
                'stateId'=>$fields['stateId'],
                'created_at' => Carbon::now()
                ]
        );

        //storeDonor
            $donorId = DB::table('tax_exemptions_donors')->insertGetId(
                [
                    'donorId' =>  $fields['donorId'],
                    'created_at' => Carbon::now()
                    ]
            );

        $postageId=null;
        //storePostageAddress
        if($fields['postId'] == "1" ){         
            if($fields['shippingId']=="false"){
                $postageId = DB::table('addresses')->insertGetId(
                    [
                        'address' =>  $fields['address'],
                        'postalCode' => $fields['postalCode'],
                        'city' => $fields['city'],
                        'stateId'=>$fields['stateId'],
                        'created_at' => Carbon::now()
                        ]
                );  
    
            }          
            else {
             $postageId = $addressId;
            }
        }
               
        $data = TaxExemption::create([
            'name' => $fields['name'],
            'phoneNumber' => $fields['phoneNumber'],
            'email' => $fields['email'],
            'amount'=>$fields['amount'],
            'fileId' => $fileId,
            'typeId' => $fields['typeId'],          
            'statusId' => 1,
            'postId' => $fields['postId'],
            'donorId' => $donorId,
            'addressId' => $addressId,
            'postageAddressId' => $postageId,
        ]);

        return $data;
    }

    public function delete($id) {
        return TaxExemption::destroy($id);
    }

    public function deleteAll($ids) {

        $array = json_decode($ids, true);
        return TaxExemption::whereIn('id', $array)->delete();
    }

    public function export(){
        
        $fileName = time().'_taxExemption.xlsx';
        $data = Excel::store(new TaxExemptionsExport, $fileName , 'excel');
        $path = '/storage/temp/'.$fileName;
        $exists = asset($path);
        return $exists;
    }

    public function updateStatus(Request $request, $id){

        $fields = $request->validate([
            'statusId' => 'required',
        ]);

        $tax = TaxExemption::find($id);
        $tax->statusId = $fields['statusId'];
        $tax->save();

        return $tax;

    }

    public function updateStatusAll(Request $request, $ids){

        $array = json_decode($ids, true);
        $fields = $request->validate([
            'statusId' => 'required',
        ]);

        return TaxExemption::whereIn('id', $array)->update(['statusId' => $fields['statusId']]);
    }
}
