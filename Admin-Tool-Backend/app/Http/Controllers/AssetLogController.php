<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\AssetItem;
use App\Models\AssetLog;
use Illuminate\Http\Request;

class AssetLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return AssetLog::all();
    }

    /**
     * Log request and update status of asset in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function request(Request $request)
    {
        $item = AssetItem::find($request->item_id);
        $status_id = $item->status_id;
        if($status_id == 1){
            $request -> validate([
                'staff_id'=> 'required',
                'item_id'=> 'required',
                'purpose'=> 'required',
                'borrowed_at'=> 'required',
            ]);
            AssetItem::where('id', $request->item_id)->update(array('status_id' => '2'));
            $item = AssetItem::find($request->item_id);
            $status_id = $item->status_id;
            return AssetLog::create($request->all());
        }else {
            return response('Asset is not Available',400);
        }
    }

    /**
     * Update status and returned_at of returned asset in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function return($id)
    {
        // $log = AssetLog::findOrFail($id);
        // $asset_id = $log->asset_id; 
        // $asset = Asset::find($asset_id);
        // $status_id = $asset->status_id;
        // if($status_id == 2){
        //     AssetLog::where('id', $id)->update(array('returned_at'=> Carbon::now()));
        //     Asset::where('id', $asset_id)->update(array('status_id' => '6'));
        //     $asset = Asset::find($asset_id);
        //     $status_id = $asset->status_id;
        //     return [AssetLog::find($id),$status_id];
        // }else {
        //     return response(400);
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return AssetLog::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $log = AssetLog::find($id);
        $log->update($request->all());
        return $log;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return AssetLog::destroy($id);
    }
}
