<?php

namespace App\Http\Controllers;

use App\Models\Asset;
use App\Models\AssetCategory;
use Illuminate\Http\Request;

class AssetCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return AssetCategory::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request -> validate([
            'category'=> 'required',
        ]);
        return AssetCategory::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return AssetCategory::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = AssetCategory::find($id);
        $category->update($request->all());
        return $category;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return AssetCategory::destroy($id);
    }

    public function showAsset()
    {
        $id = array();
        $categories = AssetCategory::all();
        for ($i=0; $i < count($categories); $i++) {
            array_push($id, $categories[$i]->id);
        };

        $numOfAssets = array();
        for ($i=0; $i < count($id); $i++) {
            array_push($numOfAssets, Asset::where('category_id', $id[$i])->get()->count());
        }
        // Asset::where('category_id', $category_id)->select('id', 'category_id','asset_name')->get()->count();$numOfAssets
        return $numOfAssets;
    }
}
