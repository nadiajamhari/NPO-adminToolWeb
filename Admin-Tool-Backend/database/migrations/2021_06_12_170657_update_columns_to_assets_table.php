<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColumnsToAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->dropForeign(['status_id']);
            $table->dropForeign(['qrcode_id']);
            $table->dropColumn(['name','code', 'price', 'status_id', 'qrcode_id']);
        });

        Schema::table('assets', function (Blueprint $table) {
            $table->string('asset_name');
            $table->string('asset_prefix');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {
            //
        });
    }
}
