<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColorIdToTaxExemptionsStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tax_exemptions_statuses', function (Blueprint $table) {
            $table->unsignedBigInteger('colorId')->nullable();             
            $table->foreign('colorId')->references('id')->on('status_colors')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tax_exemptions_statuses', function (Blueprint $table) {
            //
        });
    }
}
