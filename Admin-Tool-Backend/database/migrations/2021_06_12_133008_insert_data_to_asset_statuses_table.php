<?php

use App\Models\AssetStatus;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertDataToAssetStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_statuses', function (Blueprint $table) {
            $data = [
                ['status' => 'Available',],
                ['status' => 'In Use',],
                ['status' => 'Returned',],
                ['status' => 'Missing',],
            ];
            AssetStatus::insert($data);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_statuses', function (Blueprint $table) {
            //
        });
    }
}
