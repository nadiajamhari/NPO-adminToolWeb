<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\TaxExemptionsType;


class AddDataTypeDataToTypeTaxExemptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tax_exemptions_types', function (Blueprint $table) {
            $data = [
                ["type" => "Personal", "dataType"=>"Identification Number(IC)"],
                ["type" => "Company", "dataType"=>"Company Number"],
            ];

            TaxExemptionsType::insert($data);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tax_exemptions_types', function (Blueprint $table) {
            //
        });
    }
}
