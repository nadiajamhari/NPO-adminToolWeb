<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropDataToTaxExemptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {    
        Schema::table('tax_exemptions', function (Blueprint $table) {
            $table->dropForeign(['stateId']);
            $table->dropColumn(['identificationNumber', 'address','postalCode','city','companyNo','stateId']);           
        });
        Schema::table('tax_exemptions', function (Blueprint $table) {
            $table->unsignedBigInteger('donorId');
            $table->unsignedBigInteger('addressId');   
            $table->unsignedBigInteger('postageAddressId')->nullable()->constrained(); 
            $table->foreign('donorId')->references('id')->on('tax_exemptions_donors')->onDelete('cascade');
            $table->foreign('addressId')->references('id')->on('addresses')->onDelete('cascade');
            $table->foreign('postageAddressId')->references('id')->on('addresses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tax_exemptions', function (Blueprint $table) {
            $table->unsignedInteger('stateId');

            $table->foreign('stateId')
                  ->references('id')
                  ->on('states')
                  ->onDelete('cascade');
        });
    }
}
