<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\TypeTaxExemption;
use Illuminate\Support\Facades\DB;

class AddDataOfNameToTypeTaxExemptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tax_exemptions_types', function (Blueprint $table) {

            DB::table('tax_exemptions_types')
              ->where('id', 1)
              ->update(['name' =>'Name']);
       
            
            DB::table('tax_exemptions_types')
            ->where('id', 2)
            ->update(['name' =>'Company Name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tax_exemptions_types', function (Blueprint $table) {
            //
        });
    }
}
