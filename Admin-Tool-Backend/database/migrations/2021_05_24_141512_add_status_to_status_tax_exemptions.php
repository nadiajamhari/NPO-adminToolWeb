<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\TaxExemptionsStatus;

class AddStatusToStatusTaxExemptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tax_exemptions_statuses', function (Blueprint $table) {
            $data = [
                ["status" => "New Request"],
                ["status" => "In Progress"],
                ["status" => "Completed"],
                ["status" => "Rejected"],
            ];

            TaxExemptionsStatus::insert($data);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tax_exemptions_statuses', function (Blueprint $table) {
            //
        });
    }
}
