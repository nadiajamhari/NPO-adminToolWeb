<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\StatusColor;

class AddDataToStatusColors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('status_colors', function (Blueprint $table) {
                $data = [
                    ["color" => "primary"],
                    ["color" => "secondary"],
                    ["color" => "success"],
                    ["color" => "danger"],
                    ["color" => "warning"],
                    ["color" => "info"],
                    ["color" => "light"],
                    ["color" => "dark"],
                    ["color" => "white"],
                    ["color" => "transparent"],
                ];
    
                StatusColor::insert($data);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('status_colors', function (Blueprint $table) {
            //
        });
    }
}
