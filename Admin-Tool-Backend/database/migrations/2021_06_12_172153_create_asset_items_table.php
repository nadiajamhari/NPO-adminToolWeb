<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_items', function (Blueprint $table) {
            $table->id();
            $table->integer('item_number');
            $table->double('item_price', 8, 2);
            $table->unsignedBigInteger('asset_id');
            $table->unsignedBigInteger('status_id');
            $table->unsignedBigInteger('qrcode_id');
            $table->foreign('asset_id')->references('id')->on('assets');
            $table->foreign('status_id')->references('id')->on('asset_statuses');
            $table->foreign('qrcode_id')->references('id')->on('asset_qrcodes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_items');
    }
}
