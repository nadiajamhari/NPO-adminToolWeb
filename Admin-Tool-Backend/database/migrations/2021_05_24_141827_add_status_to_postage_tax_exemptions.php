<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\TaxExemptionsPostage;

class AddStatusToPostageTaxExemptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tax_exemptions_postages', function (Blueprint $table) {
            $data = [
                ["postStatus" => "Yes"],
                ["postStatus" => "No"],
            ];

            TaxExemptionsPostage::insert($data);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tax_exemptions_postages', function (Blueprint $table) {
            //
        });
    }
}
