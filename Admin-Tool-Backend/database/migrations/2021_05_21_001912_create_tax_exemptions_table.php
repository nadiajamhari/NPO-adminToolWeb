<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxExemptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_exemptions', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->text('name');
            $table->string('phoneNumber');
            $table->string('email');
            $table->longtext('address');
            $table->string('postalCode');
            $table->string('city');
            $table->longText('identificationNumber');
            $table->double('amount',8,2);
            $table->text('companyNo');
            $table->text('filePath');  
            $table->unsignedBigInteger('typeId');
            $table->unsignedBigInteger('statusId');   
            $table->unsignedBigInteger('postId');   
            $table->unsignedBigInteger('stateId');             
            $table->foreign('typeId')->references('id')->on('tax_exemptions_types')->onDelete('cascade');
            $table->foreign('statusId')->references('id')->on('tax_exemptions_statuses')->onDelete('cascade');
            $table->foreign('postId')->references('id')->on('tax_exemptions_postages')->onDelete('cascade');
            $table->foreign('stateId')->references('id')->on('addresses_states')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_exemptions');
    }
}
