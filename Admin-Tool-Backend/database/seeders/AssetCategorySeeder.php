<?php

namespace Database\Seeders;

use App\Models\AssetCategory;
use Illuminate\Database\Seeder;

class AssetCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'category' => 'Project Assets',
            ],
            [
                'category' => 'Media Assets',
            ],
            [
                'category' => 'Education Assets',
            ],
        ];

        foreach ($categories as $category) {
            AssetCategory::create($category);
        }
    }
}
