<?php

namespace Database\Seeders;

use App\Models\AssetStatus;
use Illuminate\Database\Seeder;

class AssetStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = [
            [
                'status' => 'Available',
            ],
            [
                'status' => 'In Use',
            ],
            [
                'status' => 'Returned',
            ],
            [
                'status' => 'Missing',
            ],
        ];

        foreach ($statuses as $status) {
            AssetStatus::create($status);
        }
        
    }
}
