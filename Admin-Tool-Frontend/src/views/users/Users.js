import React, { Component } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
  CDropdown,
  CDropdownToggle,
  CDropdownMenu,
  CDropdownItem,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CCol,
  CRow,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
} from "@coreui/react";
import api from "src/services/api";
import CIcon from "@coreui/icons-react";
import dateFormat from "dateformat";

class Users extends Component {
  constructor() {
    super();
    this.state = {
      users: [],
      id: "",
      roles: [],
      staffId: "",
      roleData: "",
      deleteModal: false,
      modalForm: false,
      name: "",
      email: "",
      roleId: 1,
      message: "",
      addUser: false,
    };
  }

  componentDidMount() {
    this.loadUsers();
    this.getRole();
  }

  loadUsers() {
    api.get("/api/allUsers").then((response) => {
      this.setState({
        users: response.data,
      });
    });
  }

  getRole() {
    api.get("/api/getRole").then((response) => {
      this.setState({
        roles: response.data,
      });
    });
  }

  updateData() {
    api.put("/api/updateUser/" + this.state.id, this.state).then((response) => {
      if (response.data.error) {
        console.log(response.data.error);
      } else {
        console.log(response.data);
        this.setState({
          modalForm: !this.state.modalForm,
          message: response.data,
          name: "",
          email: "",
          roleId: 1,
          staffId: "",
        });
      }
    });
    this.loadUsers();
  }

  deleteData(id) {
    this.setState({
      deleteModal: !this.state.deleteModal,
      id: id,
    });
  }

  modalForm(addUser, id, name, email, roleId, staffId) {
    this.setState({
      addUser: addUser,
      modalForm: !this.state.modalForm,
    });
    if (addUser === false) {
      this.setState({
        id: id,
        name: name,
        staffId: staffId,
        email: email,
        roleId: roleId,
      });
    }
  }

  confirmAdd() {
    api.post("/api/addNewUser/", this.state).then((response) => {
      if (response.data.error) {
        console.log(response.data.error);
      } else {
        console.log(response.data);
        this.setState({
          modalForm: !this.state.modalForm,
          message: response.data,
          name: "",
          email: "",
          roleId: 1,
          staffId: "",
        });
      }
    });
    this.loadUsers();
  }

  confirmDelete() {
    api.delete("/api/deleteUser/" + this.state.id).then((response) => {
      this.setState({
        deleteModal: !this.state.deleteModal,
        id: "",
      });
      this.loadUsers();
    });
  }
  render() {
    const fields = [
      "No",
      "StaffID",
      "Name",
      "Email",
      "Role",
      "RegisteredDate",
      "Action",
    ];
    const listOfUser = [];
    let number = 0;
    this.state.users.forEach((user) => {
      number = number + 1;
      let data;
      this.state.roles.forEach((role) => {
        // eslint-disable-next-line
        if (role.id === user.roleId) {
          data = role.role;
        }
      });

      listOfUser.push({
        Id: user.id,
        No: number,
        StaffID: user.staffId,
        Name: user.name,
        Email: user.email,
        RoleID: user.roleId,
        Role: data,
        RegisteredDate: dateFormat(user.created_at, "dd/mm/yyyy"),
      });
      // eslint-disable-next-line
      return;
    });

    const { name, email, staffId, roleId, addUser } = this.state;

    return (
      <div>
        <CCard>
          <CCardHeader>
            <CRow>
              {" "}
              <CCol sm={8}>
                {" "}
                <h3>List of Users</h3>{" "}
              </CCol>
              <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
                {" "}
                <CButton
                  active
                  color="info"
                  aria-pressed="true"
                  onClick={this.modalForm.bind(this, true)}
                >
                  Add User
                </CButton>
              </CCol>
            </CRow>
          </CCardHeader>
          <CCardBody>
            <CDataTable
              items={listOfUser}
              fields={fields}
              bordered
              itemsPerPage={10}
              pagination
              scopedSlots={{
                Action: (item) => (
                  <td>
                    <CDropdown>
                      <CDropdownToggle caret={false} size="sm">
                        {" "}
                        <CIcon name="cil-options" className="toggle" />{" "}
                      </CDropdownToggle>
                      <CDropdownMenu>
                        <CDropdownItem
                          onClick={this.modalForm.bind(
                            this,
                            false,
                            item.Id,
                            item.Name,
                            item.Email,
                            item.RoleID,
                            item.StaffID
                          )}
                        >
                          Update
                        </CDropdownItem>
                        <CDropdownItem
                          onClick={this.deleteData.bind(this, item.Id)}
                        >
                          Delete
                        </CDropdownItem>
                      </CDropdownMenu>
                    </CDropdown>
                  </td>
                ),
                //Checkbox
                // "": () => (
                //   <td>
                //     {" "}
                //     <CInputCheckbox
                //       id={listOfUser.Id}
                //       name="checkbox"
                //       value={listOfUser.Id}
                //     />
                //   </td>
                // ),
              }}
            />
          </CCardBody>
        </CCard>
        <CModal
          alignment="center"
          show={this.state.deleteModal}
          onClose={this.deleteData.bind(this)}
        >
          <CModalHeader closeButton></CModalHeader>
          <CModalBody>
            <div className="text-center m-3">
              <h2>Are You Sure?</h2>
              <p>
                Do you really want to delete there records? This process cannot
                be undone.
              </p>
            </div>
          </CModalBody>
          <CModalFooter className="d-flex justify-content-center">
            <CButton color="secondary" onClick={this.deleteData.bind(this)}>
              Cancel
            </CButton>
            <CButton color="danger" onClick={this.confirmDelete.bind(this)}>
              Delete
            </CButton>
          </CModalFooter>
        </CModal>

        <CModal
          alignment="center"
          show={this.state.modalForm}
          onClose={this.modalForm.bind(this)}
        >
          <CModalHeader closeButton>
            <CModalTitle>Add New User</CModalTitle>
          </CModalHeader>
          <CModalBody>
            {" "}
            <CRow>
              <CCol xs="12">
                <CFormGroup>
                  <CLabel htmlFor="name">Name</CLabel>
                  <CInput
                    id="name"
                    value={name}
                    onChange={(event) =>
                      this.setState({
                        name: event.target.value,
                      })
                    }
                  />
                </CFormGroup>
              </CCol>
            </CRow>
            <CRow>
              <CCol xs="12">
                <CFormGroup>
                  <CLabel htmlFor="email">Email</CLabel>
                  <CInput
                    id="email"
                    value={email}
                    onChange={(event) =>
                      this.setState({
                        email: event.target.value,
                      })
                    }
                  />
                </CFormGroup>
              </CCol>
            </CRow>
            <CRow>
              <CCol xs="6">
                <CFormGroup>
                  <CLabel htmlFor="staffId">Staff ID</CLabel>
                  <CInput
                    id="staffId"
                    value={staffId}
                    onChange={(event) =>
                      this.setState({
                        staffId: event.target.value,
                      })
                    }
                  />
                </CFormGroup>
              </CCol>
              <CCol xs="6">
                <CFormGroup>
                  <CLabel htmlFor="roleId">Role</CLabel>
                  <CSelect
                    name="role"
                    id="role"
                    value={roleId}
                    onChange={(event) =>
                      this.setState({
                        roleId: event.target.value,
                      })
                    }
                  >
                    {this.state.roles.map((role) => (
                      <option value={role.id} key={role.id}>
                        {" "}
                        {role.role}
                      </option>
                    ))}
                  </CSelect>
                </CFormGroup>
              </CCol>
            </CRow>
          </CModalBody>
          <CModalFooter>
            <CButton color="secondary" onClick={this.modalForm.bind(this)}>
              Cancel
            </CButton>
            <CButton
              color="info"
              onClick={
                addUser
                  ? this.confirmAdd.bind(this)
                  : this.updateData.bind(this)
              }
            >
              {addUser ? "Add" : "Save"}
            </CButton>
          </CModalFooter>
        </CModal>
      </div>
    );
  }
}

export default Users;
