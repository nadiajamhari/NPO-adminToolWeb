import React, { Component } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
  CCardGroup,
  CAlert,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import api from "../../../services/api";
import Loading from "../../../containers/Loader";

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      message: "",
      status: "",
    };
  }

  resetMyPassword() {
    this.setState({
      status: "loading",
    });
    api.get("/sanctum/csrf-cookie").then(() => {
      const data = {
        email: this.state.email,
      };
      api
        .post("/api/forgotPassword", data)
        .then((response) => {
          this.setState({
            message: response.data.message,
            status: "success",
            Success: true,
            email: "",
          });
        })
        .catch((error) => {
          this.setState({
            message: error.response.data.error,
            status: "fail",
          });
        });
    });
  }

  render() {
    const { status } = this.state;
    return (
      <div className="c-app c-default-layout flex-row align-items-center">
        <CContainer>
          <CRow className="justify-content-center">
            <CCol md="9" lg="6" xl="5">
              {status === "loading" && <Loading />}
              {status === "success" && (
                <CAlert color="success">{this.state.message}</CAlert>
              )}
              {status === "fail" && (
                <CAlert color="danger">{this.state.message}</CAlert>
              )}
              {status !== "loading" && (
                <CCardGroup>
                  <CCard className="p-4">
                    <CCardBody>
                      <CForm>
                        <h1>Forgot Password</h1>
                        <p className="text-muted">
                          Enter your email address that you used to register.
                          We'll send you an email with a link to reset your
                          password.
                        </p>
                        <CInputGroup className="mb-4 mt-4">
                          <CInputGroupPrepend>
                            <CInputGroupText>
                              <CIcon name="cil-envelope-closed" />
                            </CInputGroupText>
                          </CInputGroupPrepend>
                          <CInput
                            type="text"
                            placeholder="Email"
                            autoComplete="email"
                            value={this.state.email}
                            onChange={(event) =>
                              this.setState({
                                email: event.target.value,
                              })
                            }
                          />
                        </CInputGroup>
                        <CButton
                          color="primary"
                          block
                          onClick={this.resetMyPassword.bind(this)}
                        >
                          SEND RESET LINK PASSWORD
                        </CButton>
                      </CForm>
                    </CCardBody>
                  </CCard>
                </CCardGroup>
              )}
            </CCol>
          </CRow>
        </CContainer>
      </div>
    );
  }
}

export default ForgotPassword;
