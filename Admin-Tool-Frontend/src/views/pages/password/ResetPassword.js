import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
  CCardGroup,
  CAlert,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import api from "../../../services/api";
import Loading from "../../../containers/Loader";

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      message: "",
      status: "",
      password: "",
      confirmPassword: "",
      isSuccess: false,
      redirect: false,
    };
  }

  resetPassword() {
    this.setState({
      status: "loading",
    });
    if (this.state.confirmPassword === this.state.password) {
      api.get("/sanctum/csrf-cookie").then(() => {
        const data = {
          token: this.props.match.params.id,
          password: this.state.password,
        };
        api
          .post("/api/resetPassword", data)
          .then((response) => {
            this.setState({
              message: response.data.message,
              status: "success",
              Success: true,
            });
            this.timeout = setTimeout(
              () => this.setState({ redirect: true }),
              5000
            );
          })
          .catch((error) => {
            this.setState({
              message: error.response.data.error,
              status: "fail",
            });
          });
      });
    } else {
      this.setState({
        status: "fail",
        message: "Confirm Password not Match to New Password",
      });
    }
  }

  render() {
    const { status, isSuccess, redirect } = this.state;
    return (
      <div className="c-app c-default-layout flex-row align-items-center">
        <CContainer>
          <CRow className="justify-content-center">
            <CCol md="9" lg="6" xl="5">
              {status === "loading" && <Loading />}
              {status === "success" && (
                <CAlert currentAlertCounter="10" color="success">
                  {this.state.message}
                </CAlert>
              )}
              {status === "fail" && (
                <CAlert color="danger">{this.state.message}</CAlert>
              )}
              {status !== "loading" && (
                <CCardGroup>
                  {redirect === true && <Redirect to="/login" />}
                  <CCard className="p-4">
                    <CCardBody>
                      <CForm>
                        <h1>Reset Password</h1>
                        <CInputGroup className="mb-4 mt-4">
                          <CInputGroupPrepend>
                            <CInputGroupText>
                              <CIcon name="cil-lock-locked" />
                            </CInputGroupText>
                          </CInputGroupPrepend>
                          <CInput
                            type="password"
                            placeholder="New Password"
                            autoComplete={
                              isSuccess ? "current-password" : "off"
                            }
                            onChange={(event) =>
                              this.setState({
                                password: event.target.value,
                              })
                            }
                          />
                        </CInputGroup>
                        <CInputGroup className="mb-4">
                          <CInputGroupPrepend>
                            <CInputGroupText>
                              <CIcon name="cil-lock-locked" />
                            </CInputGroupText>
                          </CInputGroupPrepend>
                          <CInput
                            type="password"
                            placeholder="Confirm Password"
                            autoComplete={
                              isSuccess ? "current-password" : "off"
                            }
                            onChange={(event) =>
                              this.setState({
                                confirmPassword: event.target.value,
                              })
                            }
                          />
                        </CInputGroup>
                        <CButton
                          color="primary"
                          block
                          onClick={this.resetPassword.bind(this)}
                        >
                          SUBMIT
                        </CButton>
                      </CForm>
                    </CCardBody>
                  </CCard>
                </CCardGroup>
              )}
            </CCol>
          </CRow>
        </CContainer>
      </div>
    );
  }
}

export default ForgotPassword;
