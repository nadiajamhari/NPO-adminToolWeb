import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import api from "../../../services/api";
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      toHome: null,
      showAlert: false,
    };
  }

  componentDidMount() {
    //ada error kat sini
    // console.log(this.props.data, this.state);
    // if (this.props.location.state.reason) {
    //   this.setState({ showAlert: true });
    // }
  }

  handleEmail(e) {
    this.setState({ email: e.target.value });
  }

  handlePassword(e) {
    this.setState({ password: e.target.value });
  }

  handleSubmit() {
    api.get("/sanctum/csrf-cookie").then(() => {
      const creds = {
        email: this.state.email,
        password: this.state.password,
      };
      api.post("/api/login", creds).then((res) => {
        if (res.data.error) {
          console.log(res.data.error);
        } else {
          this.props.login(res.data.token);
          localStorage.setItem("token", res.data.token);
          localStorage.setItem("refreshAfterLogin", false);
          localStorage.setItem("users", JSON.stringify(res.data.user));
          this.setState({ toHome: "/dashboard" });
        }
      });
    });
  }

  render() {
    if (this.state.toHome) {
      return <Redirect to={this.state.toHome} />;
    }
    return (
      <div className="c-app c-default-layout flex-row align-items-center">
        <CContainer>
          <CRow className="justify-content-center">
            <CCol md="5">
              {this.state.showAlert && (
                <div className="alert alert-warning">
                  {this.props.location.state.reason}
                </div>
              )}
              <CCardGroup>
                <CCard className="p-4">
                  <CCardBody>
                    <CForm>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <CInputGroup className="mb-3">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <CIcon name="cil-user" />
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <CInput
                          type="text"
                          placeholder="Email"
                          autoComplete="email"
                          onChange={this.handleEmail.bind(this)}
                        />
                      </CInputGroup>
                      <CInputGroup className="mb-4">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <CIcon name="cil-lock-locked" />
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <CInput
                          type="password"
                          placeholder="Password"
                          autoComplete="current-password"
                          onChange={this.handlePassword.bind(this)}
                        />
                      </CInputGroup>
                      <CRow>
                        <CCol xs="6">
                          <CButton
                            type="button"
                            color="primary"
                            className="px-4"
                            onClick={this.handleSubmit.bind(this)}
                          >
                            Login
                          </CButton>
                        </CCol>
                        <CCol xs="6" className="text-right">
                          <CButton
                            color="link"
                            className="px-0"
                            to="/forgot-password"
                          >
                            Forgot password?
                          </CButton>
                        </CCol>
                      </CRow>
                    </CForm>
                  </CCardBody>
                </CCard>
              </CCardGroup>
            </CCol>
          </CRow>
        </CContainer>
      </div>
    );
  }
}

export default Login;
