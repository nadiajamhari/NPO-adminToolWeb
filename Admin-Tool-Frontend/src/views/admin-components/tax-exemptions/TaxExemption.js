import React, { Component } from "react";
import { CCard } from "@coreui/react";
import Table from "./TaxExemptionTable";

class TaxExemption extends Component {
  constructor() {
    super();
    this.state = {
      fileUrl: null,
    };
  }

  // exportExcel() {
  //   api.get("/api/exportExcelTax").then((response) => {
  //     this.setState(
  //       {
  //         fileUrl: response.data,
  //       },
  //       () => {
  //         if (this.state.fileUrl) {
  //           window.location.href = this.state.fileUrl;
  //         }
  //       }
  //     );
  //   });
  // }

  render() {
    return (
      <div>
        <CCard>
          <Table />
        </CCard>
      </div>
    );
  }
}

export default TaxExemption;
