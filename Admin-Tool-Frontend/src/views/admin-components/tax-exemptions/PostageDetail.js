import React, { Component } from "react";
import { CRow, CCol } from "@coreui/react";

class PostageDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    if (this.props.postStatus === false) {
      return (
        <div>
          <p className="text-muted">
            The data is unavailable since the donor did not request for physical
            receipts
          </p>
        </div>
      );
    }
    return (
      <div>
        <CRow>
          <strong>Address:</strong>
        </CRow>
        <CRow className="py-1">
          {this.props.addressShipping}, {this.props.cityShipping}
        </CRow>
        <CRow>
          <CCol>
            <CRow>
              <strong>State:</strong>
            </CRow>{" "}
            <CRow className="py-1">{this.props.stateShipping}</CRow>
          </CCol>
          <CCol>
            <CRow>
              <strong>PostalCode:</strong>
            </CRow>{" "}
            <CRow className="py-1">{this.props.postalCodeShipping}</CRow>
          </CCol>
        </CRow>
      </div>
    );
  }
}

export default PostageDetail;
