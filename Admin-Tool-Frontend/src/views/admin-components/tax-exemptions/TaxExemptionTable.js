import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import {
  CCardHeader,
  CCardBody,
  CDataTable,
  CBadge,
  CInputCheckbox,
  CRow,
  CCol,
} from "@coreui/react";
import api from "../../../services/api";
import dateFormat from "dateformat";
// import GetBadge from "./TaxExemptionStatusBadge";
import swal from "sweetalert2";
import StatusOption from "./StatusOption";
import Loader from "../../../containers/Loader";

let pushArray = [];
class TaxExemption extends Component {
  _isMounted = false;
  constructor() {
    super();
    this.state = {
      statusId: "",
      taxExemptions: [],
      colorLists: [],
      modalStatus: false,
      detailPage: null,
      checkedItems: new Map(),
      isLoading: false,
    };

    this.detailPage = this.detailPage.bind(this);
    this.disableOnRowClick = this.disableOnRowClick.bind(this);
    this.handleSelectStatus = this.handleSelectStatus.bind(this);
    this.selectChange = this.selectChange.bind(this);
  }
  componentDidMount() {
    this._isMounted = true;
    this.loadTaxExemption();
    this.getStatusColor();
  }

  getStatusColor() {
    api.get("/api/statusColor").then((response) => {
      this.setState({
        colorLists: response.data,
      });
    });
  }

  disableOnRowClick(e) {
    e.stopPropagation();
  }

  selectChange(e) {
    const item = e.target.name;
    const isChecked = e.target.checked;
    this.setState((prevState) => ({
      checkedItems: prevState.checkedItems.set(item, isChecked),
    }));
    if (isChecked === true) {
      pushArray.push(e.target.name);
    } else {
      if (pushArray.length > 0) {
        const toRemove = e.target.name;
        const index = pushArray.indexOf(toRemove);
        if (index > -1) {
          //Make sure item is present in the array, without if condition, -n indexes will be considered from the end of the array.
          pushArray.splice(index, 1);
        }
      }
    }
  }

  loadTaxExemption() {
    this.setState({
      isLoading: true,
    });
    api.get("/api/taxExemptions").then((response) => {
      this.setState({
        taxExemptions: response.data,
        taxTable: true,
        isLoading: false,
      });
    });
  }

  deleteTax() {
    const data = JSON.stringify(pushArray);
    swal
      .fire({
        title: "Are you sure?",
        text: "You won't be able to revert the tax",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      })
      .then((result) => {
        if (result.isConfirmed) {
          api.delete("/api/deleteTaxs/" + data).then(() => {
            swal
              .fire({
                title: "Deleted!",
                text: "Tax has been deleted.",
                icon: "success",
                showConfirmButtonColor: false,
                timer: 2000,
              })
              .then(() => {
                pushArray = [];
                this.loadTaxExemption();
              });
          });
        }
      })
      .catch((error) => {
        swal.fire({
          title: "Error",
          text: error,
          icon: "error",
        });
      });
  }

  handleSelectStatus(id) {
    this.updateStatus(id);
  }

  updateStatus(id) {
    this.setState({
      isLoading: true,
    });
    const ids = JSON.stringify(pushArray);
    const data = {
      statusId: id,
    };
    api.put("/api/updateStatusTaxs/" + ids, data).then(() => {
      swal
        .fire({
          title: "Done!",
          text: "Status updated",
          icon: "success",
          timer: 2000,
          showConfirmButton: false,
        })
        .then(() => {
          pushArray = [];
          this.loadTaxExemption();
        });
    });
  }

  detailPage(id) {
    this.setState({
      detailPage: "/tax-exemption/tax-exemption-details/" + id,
    });
  }

  render() {
    const { isLoading } = this.state;
    if (this.state.detailPage) {
      return <Redirect to={this.state.detailPage} />;
    }
    const fields = [
      "",
      "RequestID",
      "Name",
      "RequestDate",
      "PhoneNumber",
      "Amount",
      "Type",
      "Status",
    ];
    const listOfTax = [];
    this.state.taxExemptions.forEach((tax) => {
      let colorStatus;
      this.state.colorLists.forEach((color) => {
        if (color.id === tax.tax_exemptions_status.colorId) {
          colorStatus = color.color;
        }
      });
      listOfTax.push({
        RequestID: tax.id,
        Name: tax.name,
        PhoneNumber: tax.phoneNumber,
        RequestDate: dateFormat(tax.created_at, "dd/mm/yyyy"),
        Amount: parseFloat(tax.amount).toFixed(2),
        Type: tax.tax_exemptions_type.type,
        Status: tax.tax_exemptions_status.status,
        ColorStatus: colorStatus,
      });
    });
    return (
      <>
        <CCardHeader>
          <CRow>
            <CCol sm={8}>
              <h3>List of Tax Exemption Request</h3>
            </CCol>
            <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
              <StatusOption
                deleteTax={this.deleteTax}
                handleSelectStatus={this.handleSelectStatus}
                pushArray={pushArray.length}
              />
              {/* <CButtonGroup role="group" aria-label="Basic outlined example">
                  <CButton
                    color="info"
                    variant="outline"
                    onClick={this.exportExcel}
                  >
                    Export to Excel
                  </CButton>
                  <CButton color="info" variant="outline">
                    Export to PDF
                  </CButton>
                </CButtonGroup> */}
            </CCol>
          </CRow>
        </CCardHeader>
        <CCardBody>
          {" "}
          {isLoading === true && <Loader />}
          {isLoading === false && (
            <CDataTable
              tableFilter
              sorter
              items={listOfTax}
              fields={fields}
              bordered
              itemsPerPageSelect
              itemsPerPage={10}
              pagination
              hover
              clickableRows
              onRowClick={(item) => this.detailPage(item.RequestID)}
              // columnHeaderSlot={{
              //   "": (
              //     <CInputCheckbox
              //       id="selectAll"
              //       name="selectAll"
              //       style={{ marginLeft: "0rem" }}
              //       onClick={this.testing.bind(this)}
              //     />
              //   ),
              // }}
              scopedSlots={{
                "": (item) => (
                  <td onClick={this.disableOnRowClick}>
                    <CInputCheckbox
                      id={item.RequestID}
                      name={item.RequestID}
                      value={item.RequestID}
                      onChange={this.selectChange}
                      style={{ marginLeft: "0rem" }}
                    />
                  </td>
                ),
                Status: (item) => (
                  <td>
                    <CBadge color={item.ColorStatus}>{item.Status}</CBadge>
                  </td>
                ),
              }}
            />
          )}
        </CCardBody>
      </>
    );
  }
}

export default TaxExemption;
