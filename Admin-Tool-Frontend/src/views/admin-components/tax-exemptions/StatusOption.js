import React, { Component } from "react";
import api from "../../../services/api";
import {
  CDropdown,
  CDropdownToggle,
  CDropdownMenu,
  CDropdownItem,
} from "@coreui/react";

class StatusOption extends Component {
  constructor(props) {
    super(props);
    this.state = {
      statusList: [],
      currentStatus: "",
    };
  }

  componentDidMount() {
    this.getStatus();
  }

  getStatus() {
    api.get("api/statusTaxExemptions").then((response) => {
      this.setState({
        statusList: response.data,
        currentStatus: this.props.status,
      });
    });
  }
  changeStatus(id, status) {
    this.props.handleOptionStatus(id, status);
  }

  changeSelectStatus(id) {
    this.props.handleSelectStatus(id);
  }

  render() {
    const options = [];
    this.state.statusList.forEach((item) => {
      options.push({
        value: item.id,
        label: item.status,
      });
    });
    if (this.props.taxDetail) {
      return (
        <div>
          <CDropdown className="m-1 btn-group">
            <CDropdownToggle
              name="statusId"
              value={this.props.statusId}
              color={this.props.statusColor}
            >
              {this.props.status}
            </CDropdownToggle>
            <CDropdownMenu>
              {this.state.statusList.map((item) => (
                <CDropdownItem
                  value={item.id}
                  key={item.id}
                  onClick={this.changeStatus.bind(this, item.id, item.status)}
                >
                  {item.status}
                </CDropdownItem>
              ))}
            </CDropdownMenu>
          </CDropdown>
        </div>
      );
    } else {
      return (
        <>
          <CDropdown className="px-2">
            <CDropdownToggle
              caret={true}
              size="xl"
              variant="outline"
              color="dark"
              disabled={this.props.pushArray === 0}
            >
              {" "}
              Update Status{" "}
            </CDropdownToggle>
            <CDropdownMenu>
              {this.state.statusList.map((item) => (
                <CDropdownItem
                  value={item.id}
                  key={item.id}
                  onClick={this.changeSelectStatus.bind(this, item.id)}
                >
                  {item.status}
                </CDropdownItem>
              ))}
            </CDropdownMenu>
          </CDropdown>{" "}
          <CDropdown className="px-2">
            <CDropdownToggle
              caret={false}
              size="xl"
              variant="outline"
              color="danger"
              onClick={this.props.deleteTax}
              disabled={this.props.pushArray === 0}
            >
              {" "}
              Delete{" "}
            </CDropdownToggle>
          </CDropdown>{" "}
        </>
      );
    }
  }
}

export default StatusOption;
