import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CRow,
  CCol,
  CButton,
  CLink,
  CBadge,
} from "@coreui/react";
import api from "src/services/api";
import StatusOption from "./StatusOption";
import dateFormat from "dateformat";
import CIcon from "@coreui/icons-react";
import swal from "sweetalert2";
import PostageDetail from "./PostageDetail";
import Loader from "../../../containers/Loader";

const LinkStyle = {
  textDecoration: "none",
  color: "#768192",
  fontWeight: 600,
};

const fontStyle = {
  fontSize: 15,
};

const fontStyle1 = {
  fontSize: 16,
};
class TaxExemption extends Component {
  constructor(props) {
    super(props);
    this.state = {
      requestId: "",
      requestDate: "",
      name: "",
      donorId: "",
      phoneNumber: "",
      email: "",
      amount: "",
      currentStatusId: "",
      statusId: "",
      status: "",
      typeDonation: "",
      receiptDonate: null,
      ispostStatus: "",
      address: "",
      city: "",
      postalCode: "",
      state: "",
      addressShippng: "",
      cityShipping: "",
      postalCodeShipping: "",
      stateShipping: "",
      isPersonal: true,
      taxDetail: false,
      deleteStatus: false,
      loading: true,
      postStatus: false,
      colorLists: [],
      colorId: "",
      statusColor: "",
    };

    this.handleChange = this.handleChange.bind(this);
    this.toggleHover = this.toggleHover.bind(this);
    this.toggleOutHover = this.toggleOutHover.bind(this);
    this.handleOptionStatus = this.handleOptionStatus.bind(this);
  }

  componentDidMount() {
    this.getTaxDetail();
    this.getStatusColor();
  }

  getStatusColor() {
    api.get("/api/statusColor").then((response) => {
      this.setState({
        colorLists: response.data,
      });
    });
  }

  toggleHover(e) {
    e.target.style.color = "#3c4b64";
  }

  toggleOutHover(e) {
    e.target.style.color = "#768192";
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  handleOptionStatus(id, status) {
    this.setState(
      {
        statusId: id,
        status: status,
      },
      () => {
        this.updateStatus(id);
      }
    );
  }
  deleteTax() {
    swal
      .fire({
        title: "Are you sure?",
        text:
          "You won't be able to revert the tax with request ID " +
          this.state.requestId,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      })
      .then((result) => {
        if (result.isConfirmed) {
          api.delete("/api/deleteTax/" + this.state.requestId).then(() => {
            swal
              .fire({
                title: "Deleted!",
                text: "Tax has been deleted. You'll be redirect to the Tax Exemption List soon",
                icon: "success",
                showConfirmButton: false,
                timer: 2000,
              })
              .then(() => {
                this.setState({
                  deleteStatus: true,
                });
              });
          });
        }
      })
      .catch((error) => {
        swal.fire({
          title: "Error",
          text: error,
          icon: "error",
        });
      });
  }
  updateStatus(id) {
    let timerInterval;
    swal.fire({
      title: "Loading...",
      didOpen: () => {
        swal.showLoading();
      },
      willClose: () => {
        clearInterval(timerInterval);
      },
    });
    const data = {
      statusId: id,
    };
    api
      .put("/api/updateStatusTax/" + this.state.requestId, data)
      .then((response) => {
        swal
          .fire({
            title: "Done!",
            text: "Status updated",
            icon: "success",
            timer: 2000,
            showConfirmButton: false,
          })
          .then(() => {
            this.getTaxDetail();
          });
      });
  }
  getTaxDetail() {
    this.setState({
      loading: true,
    });
    api
      .get("/api/taxDetails/" + this.props.match.params.id)
      .then((response) => {
        this.setState(
          {
            requestId: this.props.match.params.id,
            requestDate: dateFormat(response.data.tax.created_at, "dd/mm/yyyy"),
            name: response.data.tax.name,
            donorId: response.data.tax.tax_exemptions_donor.donorId,
            phoneNumber: response.data.tax.phoneNumber,
            email: response.data.tax.email,
            amount: parseFloat(response.data.tax.amount).toFixed(2),
            currentStatusId: response.data.status.id,
            statusId: response.data.status.id,
            statusColor: response.data.status.status_color.color,
            status: response.data.status.status,
            typeDonation: response.data.tax.tax_exemptions_type.type,
            receiptDonate: response.data.fileUrl,
            ispostStatus: response.data.tax.tax_exemptions_postage.postStatus,
            address: response.data.address.address,
            city: response.data.address.city,
            postalCode: response.data.address.postalCode,
            state: response.data.address.addresses_state.state,
            taxDetail: true,
            loading: false,
          },
          () => {
            if (this.state.ispostStatus === "Yes") {
              this.setState({
                postStatus: true,
              });
            }
          }
        );

        if (response.data.addressShipping !== null) {
          this.setState({
            addressShippng: response.data.addressShipping.address,
            cityShipping: response.data.addressShipping.city,
            postalCodeShipping: response.data.addressShipping.postalCode,
            stateShipping: response.data.addressShipping.addresses_state.state,
          });
        }

        if (response.data.tax.tax_exemptions_type.type !== "Personal") {
          this.setState({
            isPersonal: false,
          });
        }
      });
  }

  render() {
    if (this.state.deleteStatus) {
      return <Redirect to="/tax-exemption" />;
    }
    if (this.state.loading) {
      return <Loader />;
    }
    const {
      requestId,
      requestDate,
      name,
      donorId,
      phoneNumber,
      email,
      amount,
      status,
      typeDonation,
      receiptDonate,
      postStatus,
      address,
      city,
      postalCode,
      state,
      isPersonal,
      addressShippng,
      cityShipping,
      postalCodeShipping,
      stateShipping,
    } = this.state;
    return (
      <div>
        <CRow>
          <CCol sm={8}>
            <CCard>
              <CCardHeader>
                {" "}
                <h4>Donor Details</h4>
              </CCardHeader>
              <CCardBody>
                <div style={fontStyle1}>
                  <CRow className="py-1">
                    <CCol sm={4}>
                      {" "}
                      <strong>{isPersonal ? "Name" : "Company Name"}</strong>
                    </CCol>
                    <strong>:</strong>
                    <CCol sm={4}>{name}</CCol>
                  </CRow>
                  <CRow className="py-1">
                    <CCol sm={4}>
                      {" "}
                      <strong>
                        {isPersonal
                          ? "Identification Number"
                          : "Company Number"}
                      </strong>
                    </CCol>
                    <strong>:</strong>
                    <CCol sm={4}>{donorId}</CCol>
                  </CRow>
                  <CRow className="py-1">
                    <CCol sm={4}>
                      {" "}
                      <strong>Phone Number </strong>
                    </CCol>
                    <strong>:</strong>
                    <CCol sm={4}>{phoneNumber}</CCol>
                  </CRow>
                  <CRow className="py-1">
                    <CCol sm={4}>
                      {" "}
                      <strong>Email</strong>
                    </CCol>
                    <strong>:</strong>
                    <CCol sm={4}>{email}</CCol>
                  </CRow>
                  <CRow className="py-1">
                    <CCol sm={4}>
                      {" "}
                      <strong> Address</strong>
                    </CCol>
                    <strong>:</strong>
                    <CCol sm={4}>
                      {" "}
                      {address}, {city}
                    </CCol>
                  </CRow>
                  <CRow className="py-1">
                    <CCol sm={4}>
                      {" "}
                      <strong> State</strong>
                    </CCol>
                    <strong>:</strong>
                    <CCol sm={4}>{state}</CCol>
                  </CRow>
                  <CRow className="py-1">
                    <CCol sm={4}>
                      {" "}
                      <strong>Postal Code</strong>
                    </CCol>
                    <strong>:</strong>
                    <CCol sm={4}>{postalCode}</CCol>
                  </CRow>
                </div>
              </CCardBody>
            </CCard>
            <CCard>
              <CCardHeader>
                <h4>Reply</h4>
              </CCardHeader>
              <CCardBody>Reply Process would be here</CCardBody>
            </CCard>
          </CCol>
          <CCol sm={4}>
            <CCard>
              <CCardBody>
                <div className="px-4 py-2" style={fontStyle}>
                  <CRow>
                    <strong>Request ID:</strong> {requestId}
                  </CRow>
                  <CRow className="py-1">
                    <CBadge color={this.state.statusColor}>{status}</CBadge>
                  </CRow>
                  <CRow>
                    <strong>Donation Type:</strong>
                  </CRow>
                  <CRow className="py-1">{typeDonation}</CRow>
                  <CRow>
                    <strong> Amount:</strong>
                  </CRow>
                  <CRow className="py-1">RM {amount}</CRow>
                  <CRow>
                    <strong> Attachment:</strong>
                  </CRow>
                  <CRow className="py-1">
                    {" "}
                    <CButton color="light">
                      <CLink
                        href={receiptDonate}
                        style={LinkStyle}
                        target="_blank"
                        onMouseEnter={this.toggleHover}
                        onMouseLeave={this.toggleOutHover}
                      >
                        {" "}
                        <CIcon name="cil-file" className="mfe-2" />
                        Donation Receipt
                      </CLink>
                    </CButton>
                  </CRow>
                  <CRow>
                    <strong> Request Received At:</strong>
                  </CRow>
                  <CRow className="py-1">{requestDate}</CRow>
                </div>
              </CCardBody>
            </CCard>
            <CCard>
              <CCardHeader>
                <h5>Status Update</h5>
              </CCardHeader>
              <CCardBody>
                <div className="px-4" style={fontStyle}>
                  <CRow>
                    <strong>Request Status:</strong>
                  </CRow>
                  <CRow className="py-1">
                    <StatusOption
                      statusId={this.state.statusId}
                      status={this.state.status}
                      handleOptionStatus={this.handleOptionStatus}
                      taxDetail={this.state.taxDetail}
                      statusColor={this.state.statusColor}
                    />
                  </CRow>
                </div>
              </CCardBody>
            </CCard>
            <CCard>
              <CCardHeader>
                <h5>Postage Details</h5>
              </CCardHeader>
              <CCardBody>
                <div className="px-4" style={fontStyle}>
                  <PostageDetail
                    postStatus={postStatus}
                    addressShipping={addressShippng}
                    cityShipping={cityShipping}
                    postalCodeShipping={postalCodeShipping}
                    stateShipping={stateShipping}
                  />
                </div>
              </CCardBody>
            </CCard>
            <CCard>
              <CCardBody>
                <CRow>
                  <CCol sm={5}>
                    <h5>Delete Tax</h5>
                  </CCol>
                  <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
                    <CButton
                      size="sm"
                      color="danger"
                      onClick={this.deleteTax.bind(this)}
                    >
                      Delete
                    </CButton>
                  </CCol>
                </CRow>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </div>
    );
  }
}

export default TaxExemption;
