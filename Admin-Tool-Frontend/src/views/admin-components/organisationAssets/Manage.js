import React, { Component } from "react";
import { CRow, CCol } from "@coreui/react";
import Category from "./Category";
import Asset from "./Asset";

class ManagePage extends Component {
  state = {};
  render() {
    return (
      <div>
        <CRow>
          <CCol xl={6}><Category/></CCol>
          <CCol xl={6}><Asset/></CCol>
        </CRow>
      </div>
    );
  }
}

export default ManagePage;
