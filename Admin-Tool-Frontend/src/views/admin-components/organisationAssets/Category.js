import React, { Component } from "react";
import {
  CButton,
  CDataTable,
  CCard,
  CCardHeader,
  CCardBody,
  CRow,
  CCol,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CForm,
  CInput,
  CInputGroup,
  CLabel,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import api from "src/services/api";
import swal from "sweetalert2";

class Category extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: [],
      assets: [],
      showAddModal: false,
      isAddMode: true,
      category_id: "",
      category_name: "",
    };

    this.setModal = this.setModal.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
  }

  componentDidMount() {
    this.loadCategories();
    this.loadCategoryAssets();
  }

  loadCategories() {
    api.get("/api/getCategory").then((res) => {
      this.setState({
        categories: res.data,
      });
    });
  }

  loadCategoryAssets() {
    api.get("/api/getCategoryAsset").then((res) => {
      this.setState({
        assets: res.data,
      });
    });
  }

  setModal(action, id, name) {
    this.setState((prevState) => ({ showAddModal: !prevState.showAddModal }));

    // Edit Asset Modal
    if (action === false) {
      this.setState({ isAddMode: false });
      this.setState({ category_id: id });
      this.setState({ category_name: name });
    }

    // Add New Asset Modal
    else {
      this.setState({ isAddMode: true });
      this.setState({ category_id: "" });
      this.setState({ category_name: "" });
    }
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleAdd() {
    var data = { category: this.state.category_name };
    api
      .post("/api/addCategory", data)
      .then(() => {
        this.setModal();
        swal
          .fire({
            title: "Done!",
            text: data.category + " category created successfully!",
            icon: "success",
            confirmButtonColor: "#5bc0de",
          })
          .then(() => {
            window.location.reload();
          });
      })
      .catch((error) => {
        this.setModal();
        swal.fire({
          title: "Error",
          text: error,
          icon: "error",
          confirmButtonColor: "#5bc0de",
        });
      });
  }

  handleDelete(id, name) {
    swal
      .fire({
        title: "Are you sure?",
        html: "You will lose every asset and items <br> in <b>" + name + "</b> category!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      })
      .then((result) => {
        if (result.isConfirmed) {
          api.delete("/api/deleteCategory/" + id).then(() => {
            window.location.reload();
          }).catch((error) => {
        swal.fire({
          title: "Error",
          html: "Please delete related asset and items <br> to this category first.",
          icon: "error",
        });
      });
        }
      })

  }

  handleUpdate() {
    const data = {
      category: this.state.category_name,
    };

    api
      .put("/api/updateCategory/" + this.state.category_id, data)
      .then(() => {
        this.setModal();
        swal
          .fire({
            title: "Done!",
            text: "Category name updated to " + data.category + ".",
            icon: "success",
            confirmButtonColor: "#5bc0de",
          })
          .then(() => {
            window.location.reload();
          });
      })
      .catch((error) => {
        console.log("update", this.state.category_id, data);
        console.log(error);
        this.setModal();
        swal
          .fire({
            title: "Error",
            text: error,
            icon: "error",
            confirmButtonColor: "#5bc0de",
          })
          .then(() => {
            window.location.reload();
          });
      });
  }

  render() {
    const fields = ["Category", "Action"];
    const list = [];
    var assetNumber = "";
    // const categoryButtons = [];
    // console.log(this.state);

    this.state.categories.forEach((category) => {
      // for table

      // for (let index = 0; index < this.state.assets.length; index++) {

      // }

      // this.state.assets.forEach((asset) => {
      //   assetNumber = asset;
      //   console.log(assetNumber);
      // })

      list.push({
        CategoryID: category.id,
        Category: category.category,
        Assets: assetNumber,
      });

      // for buttons

      // categoryButtons.push(
      //   <CCol key={category.id}>
      //     <CButton
      //       color="dark"
      //       style={{ width: "100px", height: "100px" }}
      //       key={category.id}
      //     >
      //       {category.category}
      //     </CButton>
      //   </CCol>
      // );
    });

    return (
      <div>
        <CCard>
          <CCardHeader>
            <CRow>
              <CCol sm={8}>
                <h3>Department</h3>
              </CCol>
              <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
                <CButton color="info" variant="outline" onClick={this.setModal}>
                  Add Category
                </CButton>
              </CCol>
            </CRow>
          </CCardHeader>
          <CCardBody>
            {/* <CRow>{categoryButtons}</CRow> */}
            <CDataTable
              fields={fields}
              items={list}
              scopedSlots={{
                Action: (item) => (
                  <td>
                    <CButton
                      color="info"
                      variant="outline"
                      onClick={this.setModal.bind(
                        this,
                        false,
                        item.CategoryID,
                        item.Category
                      )}
                    >
                      <CIcon name="cil-pencil" />
                    </CButton>{" "}
                    &nbsp;
                    <CButton
                      color="danger"
                      variant="outline"
                      onClick={this.handleDelete.bind(
                        this,
                        item.CategoryID,
                        item.Category
                      )}
                    >
                      <CIcon name="cil-trash" />
                    </CButton>
                  </td>
                ),
              }}
            ></CDataTable>
          </CCardBody>
        </CCard>

        <CModal show={this.state.showAddModal} onClose={this.setModal}>
          <CModalHeader closeButton>
            <CModalTitle>New Category</CModalTitle>
          </CModalHeader>
          <CModalBody>
            <CForm>
              <CInputGroup className="mb-4">
                <CInput
                  type="hidden"
                  name="category_id"
                  value={this.state.category_id}
                  // onChange={this.handleChange}
                />
              </CInputGroup>
              <CInputGroup className="mb-4">
                <CCol sm="2" className="p-1">
                  <CLabel>Name</CLabel>
                </CCol>
                <CInput
                  type="text"
                  placeholder="Name of New Category"
                  name="category_name"
                  value={this.state.category_name}
                  onChange={this.handleChange}
                />
              </CInputGroup>
            </CForm>
          </CModalBody>
          <CModalFooter>
            <CButton
              color="success"
              onClick={
                this.state.isAddMode ? this.handleAdd : this.handleUpdate
              }
            >
              {this.state.isAddMode ? "Add" : "Save"}
            </CButton>{" "}
            <CButton color="secondary" onClick={this.setModal.bind(this)}>
              Cancel
            </CButton>
          </CModalFooter>
        </CModal>
      </div>
    );
  }
}

export default Category;
