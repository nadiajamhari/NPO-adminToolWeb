import React, { Component } from "react";
import {
  CButton,
  CDataTable,
  CCard,
  CCardHeader,
  CCardBody,
  CRow,
  CCol,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CForm,
  CInput,
  CInputGroup,
  CLabel,
  CSelect,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import api from "src/services/api";
import swal from "sweetalert2";

class Category extends Component {
  constructor(props) {
    super(props);
    this.state = {
      assets: [],
      categories: [],
      showAddModal: false,
      isAddMode: true,
      asset_id: "",
      asset_name: "",
      asset_prefix: "",
      category_id: "",
    };

    this.setModal = this.setModal.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
  }

  componentDidMount() {
    this.loadAssets();
    this.loadCategories();
  }

  loadAssets() {
    api.get("/api/getAsset").then((res) => {
      this.setState({
        assets: res.data,
      });
    });
  }

  loadCategories() {
    var selection = document.getElementById("selectCategory");
    var category_id;
    var category_name;
    api.get("/api/getCategory").then((res) => {
      this.setState({ categories: res.data });
      for (let index = 0; index < res.data.length; ++index) {
        category_id = res.data[index].id;
        category_name = res.data[index].category;
        selection.options[selection.options.length] = new Option(
          category_name,
          category_id
        );
      }
    });
  }

  setModal(action, id, name, prefix, category_id) {
    this.setState((prevState) => ({ showAddModal: !prevState.showAddModal }));

    // Edit Asset Modal
    if (action === false) {
      this.setState({ isAddMode: false });
      this.setState({ asset_id: id });
      this.setState({ asset_name: name });
      this.setState({ asset_prefix: prefix });
      this.setState({ category_id: category_id });
    }

    // Add New Asset Modal
    else {
      this.setState({ isAddMode: true });
      this.setState({ asset_id: "" });
      this.setState({ asset_name: "" });
      this.setState({ asset_prefix: "" });
      this.setState({ category_id: "" });
    }
  }

  loadSelectCategory() {
    var selection = document.getElementById("selectCategory");
    var id;
    var asset;
    var category_id;
    var category_name;
    api.get("/api/getCategory").then((res) => {
      for (let index = 0; index < res.data.length; ++index) {
        id = res.data[index].id;
        asset = res.data[index].asset_name;
        category_id = res.data[index].category_id;
        for (let index = 0; index < this.state.categories.length; index++) {
          const c = this.state.categories[index];
          if (c.id === category_id) {
            category_name = c.category;
          }
        }
        selection.options[selection.options.length] = new Option(
          category_name + " - " + asset,
          id
        );
      }
    });
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleAdd() {
    var data = {
      category_id: this.state.category_id,
      asset_name: this.state.asset_name,
      asset_prefix: this.state.asset_prefix,
    };
    // console.log(data);
    api
      .post("/api/addAsset", data)
      .then(() => {
        this.setModal();
        swal
          .fire({
            title: "Done!",
            text: "Asset - " + data.asset_name + " created successfully!",
            icon: "success",
            confirmButtonColor: "#5bc0de",
          })
          .then(() => {
            window.location.reload();
          });
      })
      .catch((error) => {
        this.setModal();
        swal.fire({
          title: "Error",
          text: error,
          icon: "error",
          confirmButtonColor: "#5bc0de",
        });
      });
  }

  handleDelete(id, name) {
    swal
      .fire({
        title: "Are you sure?",
        html: "You will lose every <b>" + name + "</b> items!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      })
      .then((result) => {
        if (result.isConfirmed) {
          api
            .delete("/api/deleteAsset/" + id)
            .then(() => {
              window.location.reload();
            })
            .catch((error) => {
              console.log(error);
              swal.fire({
                title: "Error",
                html: "Please delete items under <b>" + name + "</b> first.",
                icon: "error",
              });
            });
        }
      });
  }

  handleUpdate() {
    const data = {
      asset_name: this.state.asset_name,
      asset_prefix: this.state.asset_prefix,
      category_id: this.state.category_id,
    };

    api
      .put("/api/updateAsset/" + this.state.asset_id, data)
      .then(() => {
        this.setModal();
        swal
          .fire({
            title: "Done!",
            text: "Asset updated succesfully!",
            icon: "success",
            confirmButtonColor: "#5bc0de",
          })
          .then(() => {
            window.location.reload();
          });
      })
      .catch((error) => {
        console.log("update", this.state.category_id, data);
        console.log(error);
        this.setModal();
        swal
          .fire({
            title: "Error",
            text: error,
            icon: "error",
            confirmButtonColor: "#5bc0de",
          })
          .then(() => {
            window.location.reload();
          });
      });
  }

  render() {
    const fields = ["Category", "Asset", "Prefix", "Action"];
    const list = [];
    var category_name = "";
    this.state.assets.forEach((asset) => {
      this.state.categories.forEach((category) => {
        if (category.id === asset.category_id) {
          category_name = category.category;
        }
      });
      list.push({
        AssetID: asset.id,
        Asset: asset.asset_name,
        Prefix: asset.asset_prefix,
        CategoryID: asset.category_id,
        Category: category_name,
      });
    });

    return (
      <div>
        <CCard>
          <CCardHeader>
            <CRow>
              <CCol sm={8}>
                <h3>Assets</h3>
              </CCol>
              <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
                <CButton color="info" variant="outline" onClick={this.setModal}>
                  Add Asset
                </CButton>
              </CCol>
            </CRow>
          </CCardHeader>
          <CCardBody>
            <CDataTable
              fields={fields}
              items={list}
              scopedSlots={{
                Action: (item) => (
                  <td>
                    <CButton
                      color="info"
                      variant="outline"
                      onClick={this.setModal.bind(
                        this,
                        false,
                        item.AssetID,
                        item.Asset,
                        item.Prefix,
                        item.CategoryID
                      )}
                    >
                      <CIcon name="cil-pencil" />
                    </CButton>{" "}
                    &nbsp;
                    <CButton
                      color="danger"
                      variant="outline"
                      onClick={this.handleDelete.bind(
                        this,
                        item.AssetID,
                        item.Asset
                      )}
                    >
                      <CIcon name="cil-trash" />
                    </CButton>
                  </td>
                ),
              }}
            ></CDataTable>
          </CCardBody>
        </CCard>

        <CModal show={this.state.showAddModal} onClose={this.setModal}>
          <CModalHeader closeButton>
            <CModalTitle>Asset Details</CModalTitle>
          </CModalHeader>
          <CModalBody>
            <CForm>
              <CInputGroup className="mb-4">
                <CInput
                  type="hidden"
                  name="asset_id"
                  value={this.state.asset_id}
                />
              </CInputGroup>
              <CInputGroup className="mb-4">
                <CCol sm="3" className="p-1">
                  <CLabel>Category</CLabel>
                </CCol>
                <CSelect
                  custom
                  id="selectCategory"
                  name="category_id"
                  value={this.state.category_id}
                  onChange={this.handleChange}
                >
                  <option value="0">Please select</option>
                </CSelect>
              </CInputGroup>
              <CInputGroup className="mb-4">
                <CCol sm="3" className="p-1">
                  <CLabel>Name</CLabel>
                </CCol>
                <CInput
                  type="text"
                  placeholder="Name of New Asset"
                  name="asset_name"
                  value={this.state.asset_name}
                  onChange={this.handleChange}
                />
              </CInputGroup>
              <CInputGroup className="mb-4">
                <CCol sm="3" className="p-1">
                  <CLabel>Prefix</CLabel>
                </CCol>
                <CInput
                  type="text"
                  placeholder="Prefix of Asset for Item Code "
                  name="asset_prefix"
                  value={this.state.asset_prefix}
                  onChange={this.handleChange}
                />
              </CInputGroup>
            </CForm>
          </CModalBody>
          <CModalFooter>
            <CButton
              color="success"
              onClick={
                this.state.isAddMode ? this.handleAdd : this.handleUpdate
              }
            >
              {this.state.isAddMode ? "Add" : "Save"}
            </CButton>{" "}
            <CButton color="secondary" onClick={this.setModal.bind(this)}>
              Cancel
            </CButton>
          </CModalFooter>
        </CModal>
      </div>
    );
  }
}

export default Category;
