import React, { Component } from "react";
import {
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CCol,
  CForm,
  CLabel,
  CSelect,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
} from "@coreui/react";
import api from "src/services/api";
// import swal from "sweetalert2";

class AssetModal extends Component {
  constructor(props) {
    super(props);
    this.state = { categories: [] };
  }

  componentDidMount() {
    this.loadSelectCategory();
    this.loadSelectAsset();
    this.loadSelectStatus();
  }

  loadSelectCategory() {
    api.get("/api/getCategory").then((res) => {
      this.setState({ categories: res.data });
    });
  }

  loadSelectAsset() {
    var selection = document.getElementById("selectAsset");
    var id;
    var asset;
    var category_id;
    var category_name;
    api.get("/api/getAsset").then((res) => {
      for (let index = 0; index < res.data.length; ++index) {
        id = res.data[index].id;
        asset = res.data[index].asset_name;
        category_id = res.data[index].category_id;
        for (let index = 0; index < this.state.categories.length; index++) {
          const c = this.state.categories[index];
          if (c.id === category_id) {
            category_name = c.category;
          }
        }
        selection.options[selection.options.length] = new Option(
          category_name + " - " + asset,
          id
        );
      }
    });
  }

  loadSelectStatus() {
    var selection = document.getElementById("selectStatus");
    var id;
    var status;
    api.get("/api/getStatus").then((res) => {
      for (let index = 0; index < res.data.length; ++index) {
        id = res.data[index].id;
        status = res.data[index].status;
        selection.options[selection.options.length] = new Option(status, id);
      }
    });
  }

  render() {
    return (
      <CModal show={this.props.modal} onClose={this.props.setModal}>
        <CModalHeader closeButton>
          <CModalTitle>Item Details</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CForm>
            <CInputGroup className="mb-4">
              <CInput
                type="hidden"
                name="item_id"
                value={this.props.item_id}
                onChange={this.props.handleChange}
              />
            </CInputGroup>
            {this.props.isAddMode && (
              <div>
                {/* <CInputGroup className="mb-4">
                  <CCol sm="2" className="p-1">
                    <CLabel>Category</CLabel>
                  </CCol>
                  <CSelect
                    custom
                    id="selectCategory"
                    name="selected_category_id"
                    value={this.state.selected_category_id}
                    onChange={this.handleChangeAssets}
                  >
                    <option value="0">Please select</option>
                  </CSelect>
                </CInputGroup> */}
                <CInputGroup className="mb-4">
                  <CCol sm="3" className="p-1">
                    <CLabel>Type of Asset</CLabel>
                  </CCol>
                  <CSelect
                    custom
                    id="selectAsset"
                    name="asset_id"
                    value={this.props.asset_id}
                    onChange={this.props.handleChange}
                  >
                    <option value="0">Please select</option>
                  </CSelect>
                </CInputGroup>

                <CInputGroup className="mb-4">
                  <CCol sm="3" className="p-1">
                    <CLabel>Price</CLabel>
                  </CCol>
                  <CInputGroupPrepend>
                    <CInputGroupText>RM</CInputGroupText>
                  </CInputGroupPrepend>
                  <CInput
                    type="number"
                    placeholder="0.00"
                    // step=".1"
                    name="item_price"
                    value={this.props.item_price}
                    onChange={this.props.handleChange}
                  />
                </CInputGroup>
              </div>
            )}
            <CInputGroup className="mb-4">
              <CCol sm="3" className="p-1">
                <CLabel>Status</CLabel>
              </CCol>
              <CSelect
                custom
                id="selectStatus"
                name="status_id"
                value={this.props.status_id}
                onChange={this.props.handleChange}
              >
                <option value="0">Please select</option>
              </CSelect>
            </CInputGroup>
          </CForm>
        </CModalBody>
        <CModalFooter>
          <CButton
            color="success"
            onClick={
              this.props.isAddMode
                ? this.props.handleAdd
                : this.props.handleUpdate
            }
          >
            {this.props.isAddMode ? "Add" : "Save"}
          </CButton>{" "}
          <CButton color="secondary" onClick={this.props.setModal}>
            Cancel
          </CButton>
        </CModalFooter>
      </CModal>
    );
  }
}

export default AssetModal;
