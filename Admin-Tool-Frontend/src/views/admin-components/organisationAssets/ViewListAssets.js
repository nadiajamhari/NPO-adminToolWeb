import React, { Component } from "react";
import api from "src/services/api";
import swal from "sweetalert2";
import AssetTable from "./AssetTable";
import AssetModal from "./AssetModal";

class ViewListAssets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      assets: [],
      categories: [],
      statuses: [],
      qrcodes: [],
      qrcode_url: [],
      modal: false,
      isAddMode: true,
      item_id: "",
      asset_id: "",
      asset_name: "",
      asset_prefix: "",
      item_number: "",
      item_price: "",
      category_id: "",
      status_id: "",
      qrcode_id: "",
    };

    this.setModal = this.setModal.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this, this.state.item_id);
  }

  componentDidMount() {
    this.loadItems();
    this.loadAssets();
    this.loadCategories();
    this.loadStatuses();
    this.loadQrcodes();
  }

  loadItems() {
    api.get("/api/assetItem").then((res) => {
      this.setState({
        items: res.data,
      });
    });
  }

  loadAssets() {
    api
      .get("/api/getAsset")
      .then((res) => {
        this.setState({
          assets: res.data,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  loadCategories() {
    api.get("/api/getCategory").then((res) => {
      this.setState({
        categories: res.data,
      });
    });
  }

  loadStatuses() {
    api.get("/api/getStatus").then((res) => {
      this.setState({
        statuses: res.data,
      });
    });
  }

  loadQrcodes() {
    api.get("/api/getQrcode").then((res) => {
      this.setState({
        qrcodes: res.data,
      });
    });
  }

  setModal(action, id, status) {
    this.setState((prevState) => ({ modal: !prevState.modal }));

    // Edit Asset Modal
    if (action === false) {
      this.setState({ isAddMode: false });
      this.setState({ item_id: id });
      this.setState({ status_id: status });
    } 
    // Add Asset Modal
    else {
      this.setState({ isAddMode: true });
      this.setState({ item_id: "" });
      this.setState({ status_id: "1" });
    }
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleAdd() {
    var data = {
      asset_id: this.state.asset_id,
      item_price: this.state.item_price,
      status_id: 1,
    };
    api
      .post("/api/assetItem", data)
      .then(() => {
        this.setModal();
        swal
          .fire({
            title: "Done!",
            text: "Item added successfully!",
            icon: "success",
            confirmButtonColor: "#5bc0de",
          })
          .then(() => {
            window.location.reload();
          });
      })
      .catch((error) => {
        this.setModal();
        swal.fire({
          title: "Error",
          text: error,
          icon: "error",
          confirmButtonColor: "#5bc0de",
        });
      });
  }

  handleUpdate() {
    const data = {
      status_id: this.state.status_id,
    };
    api
      .put("/api/assetItem/" + this.state.item_id, data)
      .then(() => {
        console.log("update");
        this.setModal();
        swal
          .fire({
            title: "Done!",
            text: "Item status updated successfully!",
            icon: "success",
            confirmButtonColor: "#5bc0de",
          })
          .then(() => {
            window.location.reload();
          });
      })
      .catch((error) => {
        console.log("update");
        console.log(error);
        this.setModal();
        swal
          .fire({
            title: "Error",
            text: error,
            icon: "error",
            confirmButtonColor: "#5bc0de",
          })
          .then(() => {
            window.location.reload();
          });
      });
  }

  render() {
    const fields = [
      // "ItemID",
      "Category",
      "AssetName",
      "ItemCode",
      "ItemPrice",
      "Status",
      "Action",
    ];
    const listOfAssets = [];
    var asset_id = "";
    var asset_name = "";
    var asset_prefix = "";
    var category_id = "";
    var category_name = "";
    var status_name = "";
    var qrcode_path = "";
    this.state.items.forEach((item) => {
      this.state.assets.forEach((asset) => {
        if (asset.id === item.asset_id) {
          asset_id = asset.id;
          asset_name = asset.asset_name;
          asset_prefix = asset.asset_prefix;
          category_id = asset.category_id;
        }
      });

      this.state.categories.forEach((category) => {
        if (category.id === category_id) {
          category_name = category.category;
        }
      });

      this.state.statuses.forEach((status) => {
        if (status.id === item.status_id) {
          status_name = status.status;
        }
      });

      this.state.qrcodes.forEach((qrcode) => {
        if (qrcode.id === item.qrcode_id) {
          qrcode_path = qrcode.src;
        }
      });

      listOfAssets.push({
        ItemID: item.id,
        AssetID: asset_id,
        AssetName: asset_name,
        AssetPrefix: asset_prefix,
        ItemNumber: item.item_number,
        ItemCode: asset_prefix+("00000"+item.item_number).slice(-3),
        ItemPrice: item.item_price,
        CategoryID: category_id,
        Category: category_name,
        StatusID: item.status_id,
        Status: status_name,
        QRCodeID: item.qrcode_id,
        QRCode: qrcode_path,
      });
    });

    return (
      <div>
        <AssetTable
          listOfAssets={listOfAssets}
          fields={fields}
          setModal={this.setModal}
        />
        <AssetModal
          isAddMode={this.state.isAddMode}
          modal={this.state.modal}
          item_id={this.state.item_id}
          asset_id={this.state.asset_id}
          item_price={this.state.item_price}
          categories={this.state.categories}
          status_id={this.state.status_id}
          setModal={this.setModal}
          handleChange={this.handleChange}
          handleAdd={this.handleAdd}
          handleUpdate={this.handleUpdate}
        />
      </div>
    );
  }
}

export default ViewListAssets;
