import React, { Component } from "react";
import {
  CBadge,
  CButton,
  CDataTable,
  CCard,
  CCardHeader,
  CCardBody,
  CDropdown,
  CDropdownToggle,
  CDropdownMenu,
  CDropdownItem,
  CRow,
  CCol,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import api from "src/services/api";
import swal from "sweetalert2";

const getBadge = (status) => {
  switch (status) {
    case "Available":
      return "success";
    case "In Use":
      return "light";
    case "Returned":
      return "warning";
    case "Missing":
      return "danger";
    default:
      return "primary";
  }
};

class AssetTable extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleDelete(id, code) {
    swal
      .fire({
        title: "Are you sure?",
        html: "You won't be able to undo this!<br>Item to delete: " + code,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      })
      .then((result) => {
        if (result.isConfirmed) {
          api.delete("/api/assetItem/" + id).then(() => {
            swal
              .fire({
                title: "Deleted!",
                text: code + " has been deleted.",
                icon: "success",
                confirmButtonColor: "#5bc0de",
              })
              .then(() => {
                window.location.reload();
              });
          });
        }
      })
      .catch((error) => {
        swal.fire({
          title: "Error",
          text: error,
          icon: "error",
        });
      });
  }

  openInNewTab(url) {
    const newWindow = window.open(url, "_blank", "noopener,noreferrer");
    if (newWindow) newWindow.opener = null;
  }

  render() {
    return (
      <div className="asset-table">
        <CCard>
          <CCardHeader>
            <CRow>
              <CCol sm={8}>
                <h3>All Asset Items</h3>
              </CCol>
              <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
                <CButton color="info" onClick={this.props.setModal}>
                  Add Item
                </CButton>
              </CCol>
            </CRow>
          </CCardHeader>
          <CCardBody>
            <CDataTable
              items={this.props.listOfAssets}
              fields={this.props.fields}
              itemsPerPage={10}
              pagination
              scopedSlots={{
                Status: (item) => (
                  <td>
                    <CBadge color={getBadge(item.Status)} className="p-1">
                      {item.Status}
                    </CBadge>
                  </td>
                ),

                Action: (item) => (
                  <td>
                    <CDropdown>
                      <CDropdownToggle caret={false} size="sm">
                        <CIcon name="cil-options" className="toggle" />
                      </CDropdownToggle>
                      <CDropdownMenu>
                        <CDropdownItem
                          onClick={this.openInNewTab.bind(this, item.QRCode)}
                        >
                          View QR Code
                        </CDropdownItem>
                        <CDropdownItem
                          onClick={this.props.setModal.bind(
                            this,
                            false,
                            item.ItemID,
                            // item.AssetID,
                            // item.AssetPrefix,
                            // item.ItemNumber,
                            // item.ItemPrice,
                            // item.CategoryID,
                            item.StatusID
                          )}
                        >
                          Update Status
                        </CDropdownItem>
                        <CDropdownItem
                          onClick={this.handleDelete.bind(
                            this,
                            item.ItemID,
                            item.ItemCode
                          )}
                        >
                          Delete
                        </CDropdownItem>
                      </CDropdownMenu>
                    </CDropdown>
                  </td>
                ),
              }}
            />
          </CCardBody>
        </CCard>
      </div>
    );
  }
}

export default AssetTable;
