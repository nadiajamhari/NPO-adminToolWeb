import React, { Component } from "react";
import {
  CCard,
  CCardHeader,
  CCardBody,
  CDataTable,
  CDropdownToggle,
  CDropdown,
  CDropdownMenu,
  CDropdownItem,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CRow,
  CCol,
  CInput,
  CButton,
  CBadge,
} from "@coreui/react";
import api from "../../../../services/api";
import CIcon from "@coreui/icons-react";

class TaxRequest extends Component {
  constructor() {
    super();
    this.state = {
      statusList: [],
      statusModal: false,
      statusId: "",
      colorId: "",
      statusColor: "",
      status: "",
      colorList: [],
      colorDefault: "",
      isEdit: false,
    };
  }

  componentDidMount() {
    this.getStatus();
    this.getStatusColor();
  }

  getStatusColor() {
    api.get("/api/statusColor").then((response) => {
      this.setState(
        {
          colorList: response.data,
          colorDefault: response.data[0].color,
        },
        () => {
          console.log(this.state.colorDefault);
        }
      );
    });
  }
  getStatus() {
    api.get("api/statusTaxExemptions").then((response) => {
      this.setState({
        statusList: response.data,
      });
    });
  }

  changeColor(id, color) {
    console.log("ia?");
    this.setState({
      colorId: id,
      statusColor: color,
      colorDefault: color,
    });
  }
  updateData() {
    const data = {
      status: this.state.status,
      colorId: this.state.colorId,
    };
    api
      .put("api/updateStatusTaxRequest/" + this.state.statusId, data)
      .then((response) => {
        this.setState({
          statusModal: !this.state.statusModal,
        });
        this.getStatus();
      });
  }
  confirmAdd() {
    const data = {
      status: this.state.status,
      colorId: this.state.colorId,
    };
    api.post("/api/addStatusTaxRequest", data).then((response) => {
      this.setState({
        statusModal: !this.state.statusModal,
      });
      this.getStatus();
    });
  }
  statusModal(isEdit, id, status, color) {
    this.setState({
      statusModal: !this.state.statusModal,
      isEdit: isEdit,
      status: status,
      statusColor: color,
      statusId: id,
      defaultColor: color,
    });
  }

  render() {
    const { isEdit } = this.state;
    const fields = ["No", "Status", "Color", "Action"];
    const listOfStatus = [];
    let number = 0;
    this.state.statusList.forEach((status) => {
      number = number + 1;

      listOfStatus.push({
        Id: status.id,
        No: number,
        Status: status.status,
        Color: status.status_color.color,
      });
      // eslint-disable-next-line
      return;
    });
    return (
      <div>
        <CRow>
          <CCol xl={6}>
            {" "}
            <CCard>
              <CCardHeader>
                <CRow>
                  {" "}
                  <CCol sm={8}>
                    {" "}
                    <h3>List of Tax Request Status</h3>{" "}
                  </CCol>
                  <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
                    {" "}
                    <CButton
                      active
                      color="info"
                      aria-pressed="true"
                      onClick={this.statusModal.bind(this, false)}
                    >
                      Add Status
                    </CButton>
                  </CCol>
                </CRow>
              </CCardHeader>
              <CCardBody>
                <CDataTable
                  items={listOfStatus}
                  fields={fields}
                  bordered
                  itemsPerPage={10}
                  pagination
                  scopedSlots={{
                    Action: (item) => (
                      <td>
                        <CDropdown>
                          <CDropdownToggle caret={false} size="sm">
                            {" "}
                            <CIcon name="cil-options" className="toggle" />{" "}
                          </CDropdownToggle>
                          <CDropdownMenu>
                            <CDropdownItem
                              onClick={this.statusModal.bind(
                                this,
                                true,
                                item.Id,
                                item.Status,
                                item.Color
                              )}
                            >
                              Update
                            </CDropdownItem>
                            <CDropdownItem>Delete</CDropdownItem>
                          </CDropdownMenu>
                        </CDropdown>
                      </td>
                    ),
                    Color: (item) => (
                      <td>
                        {" "}
                        <CBadge color={item.Color}>{item.Color}</CBadge>
                      </td>
                    ),
                    //Checkbox
                    // "": () => (
                    //   <td>
                    //     {" "}
                    //     <CInputCheckbox
                    //       id={listOfUser.Id}
                    //       name="checkbox"
                    //       value={listOfUser.Id}
                    //     />
                    //   </td>
                    // ),
                  }}
                />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>

        <CModal
          alignment="center"
          show={this.state.statusModal}
          onClose={this.statusModal.bind(this)}
        >
          <CModalHeader closeButton>
            <CModalTitle>Update Tax Request Status</CModalTitle>
          </CModalHeader>
          <CModalBody>
            {" "}
            <div className="px-4">
              <CRow>Status:</CRow>
              <CRow className="py-1">
                <CInput
                  id="status"
                  name="status"
                  value={this.state.status}
                  onChange={(event) =>
                    this.setState({
                      status: event.target.value,
                    })
                  }
                />
              </CRow>
              <CRow>Color:</CRow>
              <CRow className="py-1">
                <CDropdown className="m-1 btn-group">
                  {isEdit ? (
                    <CDropdownToggle
                      name="colorStatus"
                      color={this.state.statusColor}
                    >
                      {this.state.statusColor}
                    </CDropdownToggle>
                  ) : (
                    <CDropdownToggle
                      name="colorStatus"
                      color={this.state.colorDefault}
                    >
                      {this.state.colorDefault}
                    </CDropdownToggle>
                  )}

                  <CDropdownMenu>
                    {this.state.colorList.map((item) => (
                      <CDropdownItem
                        value={item.id}
                        key={item.id}
                        onClick={this.changeColor.bind(
                          this,
                          item.id,
                          item.color
                        )}
                      >
                        {item.color}
                      </CDropdownItem>
                    ))}
                  </CDropdownMenu>
                </CDropdown>
              </CRow>
            </div>
          </CModalBody>
          <CModalFooter>
            <CButton color="secondary" onClick={this.statusModal.bind(this)}>
              Cancel
            </CButton>
            <CButton
              color="info"
              onClick={
                isEdit ? this.updateData.bind(this) : this.confirmAdd.bind(this)
              }
            >
              {isEdit ? "Save" : "Add"}
            </CButton>
          </CModalFooter>
        </CModal>
      </div>
    );
  }
}

export default TaxRequest;
