import axios from "axios";

const apiClient = axios.create({
  baseURL: "https://nadofficial.com",
  withCredentials: true,
  headers: {
    Authorization: "Bearer " + localStorage.getItem("token"),
  },
});

export default apiClient;
